# cheatsheets-reader #

Easy way to open pdf cheatsheets with autocomplete

## How to run ##

#### Use a virtual environment ####
- `virtualenv venv` 
- `source venv/bin/activate`

#### Install everything from the requirements file ####
- `pip install -r requirements.txt`
