#!/usr/bin/env python

""" cheatsheets.py : Easy way to open pdf cheatsheets

Open pdf cheatsheets with autocomplete popup for file names.
"""
from __future__ import unicode_literals
import os
from subprocess import call, CalledProcessError
from prompt_toolkit import prompt, AbortAction
from prompt_toolkit.history import InMemoryHistory
from prompt_toolkit.contrib.completers import WordCompleter
from pygments.style import Style
from pygments.token import Token
from pygments.styles.default import DefaultStyle

__author__ = "Miroslav Vidović"
__email__ = "vidovic.miroslav@yahoo.com"
__date__ = "08.02.2017."
__version__ = "0.2"
__status__ = "Production"


class DocumentStyle(Style):  # pylint: disable=too-few-public-methods
    """
    Styles for the completion menu
    """
    styles = {
        Token.Menu.Completions.Completion.Current: 'bg:#00bbbb #000000',
        Token.Menu.Completions.Completion: 'bg:#001563 #ffffff',
        Token.Menu.Completions.ProgressButton: 'bg:#003333',
        Token.Menu.Completions.ProgressBar: 'bg:#00aaaa',
    }
    styles.update(DefaultStyle.styles)


def ascii_art():
    """
    Display ASCII art in the header
    """
    print("""

        hh                     tt          hh                    tt
  cccc hh        eee    aa aa tt     sss  hh        eee    eee  tt     sss
cc     hhhhhh  ee   e  aa aaa tttt  s     hhhhhh  ee   e ee   e tttt  s
cc     hh   hh eeeee  aa  aaa tt     sss  hh   hh eeeee  eeeee  tt     sss
 ccccc hh   hh  eeeee  aaa aa  tttt     s hh   hh  eeeee  eeeee  tttt     s
                                     sss                               sss

                        """)


def create_autocomplete_dictionary(path):
    """
    Create a dictionary for autocomplete from all the files in the path
    """
    pdf_files = [f for f in os.listdir(path) if
                 f.endswith('.pdf')]

    cheat_dictionary = WordCompleter(pdf_files, ignore_case=True)
    return cheat_dictionary


def main():
    """
    Use the created dictionary and activate popup on user input.
    """
    cheat_path = "/home/miroslav/CheatSheets/"
    cheat_compleater = create_autocomplete_dictionary(cheat_path)
    history = InMemoryHistory()

    ascii_art()

    while True:
        try:
            user_input = prompt('choose a cheatsheet> ', style=DocumentStyle,
                                completer=cheat_compleater, history=history,
                                on_abort=AbortAction.RETRY)
            if user_input == "exit":
                break
            elif user_input == "":
                print("Please enter a cheatsheet name.")
            else:
                try:
                    print('You entered:', user_input)
                    call(["xdg-open", cheat_path+user_input])
                except CalledProcessError:
                    print("An error occurred while trying to open the "
                          "file: "+user_input)
                except OSError:
                    print("You need to have xdg-open installed to open the "
                          "cheatsheets.")
        except EOFError:
            break  # Control-D pressed.
    print('Good Bye!')


if __name__ == '__main__':
    main()
